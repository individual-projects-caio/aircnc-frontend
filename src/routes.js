import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Login from './screens/login/Index'
import Dashboard from './screens/dashboard/Index'
import New from './screens/new/Index'

export default function Routes () {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Login} />
      <Route path='/dashboard' component={Dashboard} />
      <Route path='/new' component={New} />
    </Switch>
    </BrowserRouter>
  )
}

