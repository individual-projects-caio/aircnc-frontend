import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import Api from '../../services/Api'
import './styles.css'

export default function Dashboard () {

  const [ spots, setSpots ] = useState([])

  useEffect (() =>  {
    async function loadSpots () {
      const user_id = localStorage.getItem('user')
      const spots = (await Api.get('/dashboard', {
        headers: { user_id }
      })).data
      setSpots(spots)
    }
    loadSpots()
  }, [])

  return (
    <>
      <ul className='spot-list'>
        { spots.map(spot => (
          <li key={spot._id}>
            <header style={{ backgroundImage: `url(${spot.thumbnail_url})` }} />
            <strong>{ spot.company }</strong>
            <span>{ spot.price ? `R$${spot.price}/dia` : 'GRATUITO' }</span>
          </li>
        )) }
      </ul>
      <Link to='/new'>
        <button className='btn'>Cadastrar novo spot</button>
      </Link>
    </>
  )

}